<?php




/**
* Display help and module information
* @param path which path of the site we're displaying help
* @param arg array that holds the current path as would be returned from arg() function
* @return help text for the path
*/
function hdconnector_help($path, $arg) {
  $output = '';  //declare your output variable
  switch ($path) {
    case "admin/help#hdconnector":
      $output = '<p>'.  t("Provides simple connection to Helpdesk system by MK Solutions") .'</p>';
      break;
  }
  return $output;
} // function hdconnector_help




/**
* Valid permissions for this module
* @return array An array of valid permissions for the onthisdate module
*/
function hdconnector_perm() {
  return array('administer hdconnector', 'use hdconnector');
} // function hdconnector_perm()



function hdconnector_admin() {
  $form = array();

  $form['hdconnector_desturl'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway URL'),
    '#default_value' => variable_get('hdconnector_desturl', ''),
    '#description' => t("Gateway URL of Helpdesk system."),
    '#required' => TRUE,
  );

  $form['hdconnector_hdurl'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of Helpdesk system.'),
    '#default_value' => variable_get('hdconnector_hdurl', ''),
    '#description' => t("Web URL of Helpdesk system."),
    '#required' => TRUE,
  );

  $form['hdconnector_dts'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta timestamp'),
    '#default_value' => variable_get('hdconnector_dts', 0),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t("Differention between time on this system and time on Helpdesk (in minutes)."),
    '#required' => TRUE,
  );

  $form['hdconnector_login'] = array(
    '#type' => 'select',
    '#title' => t('Login item'),
    '#default_value' => variable_get('hdconnector_login', ''),
    '#options' => hdconnector_getprofilefields(),
    '#description' => t("Name of user profile item with login."),
    '#required' => TRUE,
  );

  $form['hdconnector_pass'] = array(
    '#type' => 'select',
    '#title' => t('Password item'),
    '#default_value' => variable_get('hdconnector_pass', ''),
    '#options' => hdconnector_getprofilefields(),
    '#description' => t("Name of user profile item with password."),
    '#required' => TRUE,
  );

  $form['hdconnector_key'] = array(
    '#type' => 'textfield',
    '#title' => t('TwoFish crypting key'),
    '#default_value' => variable_get('hdconnector_key', ''),
    '#description' => t("Strong text key for encrypt sending data."),
    '#required' => TRUE,
  );


  return system_settings_form($form);
}

function hdconnector_admin_validate($form, &$form_state) {
  $delta = $form_state['values']['hdconnector_dts'];
  if (!is_numeric($delta)) {
    form_set_error('hdconnector_dts', t('You must enter an integer for the time difference.'));
  }
}

function hdconnector_getprofilefields() {
  $retarray = array();
  $query_result = db_query("SELECT title, name, category FROM {profile_fields}");
  while ($result = db_fetch_object($query_result)) {
    $retarray[$result->name] = $result->title . " (" . $result->category . ")";
  }
  return $retarray;
}

function hdconnector_menu() {

  $items = array();

  $items['admin/settings/hdconnector'] = array(
    'title' => t('Helpdeck Connector'),
    'description' => t('Settings of Helpdesk Connector module.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hdconnector_admin'),
    'access arguments' => array('administer hdconnector'),
    'type' => MENU_NORMAL_ITEM,
   );

  return $items;
}


/**
* Implementation of hook_block().
* @param string $op one of "list", "view", "save" and "configure"
* @param integer $delta code to identify the block
* @param array $edit only for "save" operation
*/
function hdconnector_block($op = 'list', $delta = 0, $edit = array()) { 
  if ($op == "list") {
    // Generate listing of blocks from this module, for the admin/block page
    $block = array();
    $block[0]["info"] = t('Helpdesk Connector');
    return $block;
  }
  
  
  if ($op == "view") {
    
    $logged_user = '';
    $block_content = '';
    
    $block_content = hdconnector_getform();
    $block['subject'] = 'Vstup na Helpdesk';
    if ($block_content == '') {  
       $block['content'] = 'Sorry No Content';
    }
    else {
      $block['content'] = $block_content;
    }
  
  return $block;
  }    

  
} // function hdconnector_block






function hdconnector_getuserandpass() {

  global $user;
  
  $query_result = db_query("SELECT fid FROM {profile_fields} WHERE name = '%s'", variable_get('hdconnector_login', 'profile_hdclogin'));
  $result = db_fetch_object($query_result);
  $hdclogin_fid = $result->fid; 
  
  $query_result = db_query("SELECT fid FROM {profile_fields} WHERE name = '%s'", variable_get('hdconnector_pass', 'profile_hdcpass'));
  $result = db_fetch_object($query_result);
  $hdcpass_fid = $result->fid; 
    
  //var_dump($hdclogin_fid, $hdcpass_fid);  
    
  $result = null; 
  
  $query_result = db_query("SELECT 
              users.uid AS uid,
              profile_values_profile_hdcpass.value AS pass,
              profile_values_profile_hdclogin.value AS user
            FROM {users} users 
            LEFT JOIN {profile_values} profile_values_profile_hdcpass ON 
              users.uid = profile_values_profile_hdcpass.uid 
              AND profile_values_profile_hdcpass.fid = '%d'
            LEFT JOIN {profile_values} profile_values_profile_hdclogin ON 
              users.uid = profile_values_profile_hdclogin.uid 
              AND profile_values_profile_hdclogin.fid = '%d'
            WHERE users.uid = '%s'", $hdcpass_fid, $hdclogin_fid, $user->uid);
    while ($result = db_fetch_object($query_result)) {
      return $result;
    }  
}


function hdconnector_encrypt($user, $pass) {
  $key = variable_get('hdconnector_key', '');
  //$time = substr(date("YmdHis") + variable_get('hdconnector_dts', 0), 0, 12);
  $time = date("YmdHis", time() + (variable_get('hdconnector_dts', 0) * 60)); 
  $pass = strtr($pass, array(':' => '\:', '\\' => '\\\\'));
  $data = $user . ":" . $pass . ":" . $time;
  $encrypt = base64_encode(@mcrypt_encrypt(MCRYPT_TWOFISH, $key, $data, MCRYPT_MODE_CFB));
  //var_dump($time);
  return $encrypt;
}






function hdconnector_getform() {
  
  $desturl = variable_get('hdconnector_desturl', 'http://localhost');
  $result = hdconnector_getuserandpass();
  $showbtn = true;

  if (empty($result->user) or empty($result->pass)) {
    $showbtn = false;
    drupal_set_message(t("User name or user login for Helpdesk is not defined."), 'warning');
  } else {
    $authstr = hdconnector_encrypt($result->user, $result->pass);
  }
  
  $html  = '';  
  $html .='<p>Pokud máte uživatelský přístup na web Helpdesk a tyto informace jsou uloženy v profilu tohoto webového portálu, připojíte se jednoduše kliknutím na tlačítko.</p>';
  $html .='<form action="' . $desturl . '"
  method="get">';
  $html .='<input type="hidden" name="auth" value="' . $authstr . '" />';
  if ($showbtn) {
    $html .='<input type="submit" value="Vstoupit na Helpdesk" />';
  } else {
    $html .= '<p><strong>'. t("Nelze se připojit automaticky.") . '</strong><br />';
    $html .= l(t("Přejít na přihlašovací stránku Helpdesku..."), variable_get('hdconnector_hdurl', 'http://localhost/')) . '</p>';
  }
  $html .='</form>';    
  
  return $html;
}

?>
